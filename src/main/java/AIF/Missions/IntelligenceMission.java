package AIF.Missions;

import AIF.Entities.Coordinates;

public class IntelligenceMission extends Mission{
    String region;

    public IntelligenceMission(String region, Coordinates coordinates) {
        super(coordinates);
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
