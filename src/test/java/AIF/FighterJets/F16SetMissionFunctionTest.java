package AIF.FighterJets;

import AIF.AIFUtil;
import AIF.AerialVehicles.FighterJets.F15;
import AIF.AerialVehicles.FighterJets.F16;
import AIF.Entities.Coordinates;
import AIF.Missions.AttackMission;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


public class F16SetMissionFunctionTest {
    public static AIFUtil aifUtil;
    public static F16 f16;
    public static Coordinates coordinatesToAttack;
    public static AttackMission attackMission;

    @BeforeClass
    public static void setUpBeforeClass() {
        coordinatesToAttack = new Coordinates(31.389906, 34.330190);
        attackMission = new AttackMission("suspect house", coordinatesToAttack);
        f16 = new F16("elint", 2, "Spice250", "Donald Duck", attackMission, 10, true);



        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testF16ExecutableMission() throws MissionTypeException {
        try {
            aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException missionTypeException) {//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();

        }
    }


    @Test(expected = MissionTypeException.class)
    public void testF16NotExistsMission() throws MissionTypeException {

        aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("intelligence"));
    }

    @Test
    public void testF16getHoursOfFlightSinceLastRepair()   {
        assertEquals(f16.getHoursOfFlightSinceLastRepair(), 10);
    }

}
