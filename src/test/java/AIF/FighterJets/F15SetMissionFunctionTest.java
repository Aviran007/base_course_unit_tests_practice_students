package AIF.FighterJets;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import AIF.AIFUtil;
import AIF.AerialVehicles.FighterJets.F15;
import AIF.Entities.Coordinates;
import AIF.Missions.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class F15SetMissionFunctionTest {
    public static AIFUtil aifUtil;
    public static F15 f15;
    public static Coordinates coordinatesToAttack;
    public static AttackMission attackMission;

    @BeforeClass
    public static void setUpBeforeClass() {
        coordinatesToAttack = new Coordinates(31.389906, 34.330190);
        attackMission = new AttackMission("suspect house", coordinatesToAttack);
        f15 = new F15("elint", 2, "Spice250", "Donald Duck", attackMission, 10, true);


        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testF15ExecutableMission() throws MissionTypeException {
        try {
            aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException missionTypeException) {//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();

        }
    }

    @Test(expected = MissionTypeException.class)
    public void testF15NotExistsMission() throws MissionTypeException {

        aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get(""));
        assertTrue(true);
    }

    @Test
    public void testF15getSensorType() {
        assertEquals(f15.getSensorType(), "elint");

    }


}
