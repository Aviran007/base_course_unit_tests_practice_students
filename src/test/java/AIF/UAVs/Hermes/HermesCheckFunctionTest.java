package AIF.UAVs.Hermes;

//incomplete ...

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;

import AIF.AIFUtil;
import AIF.AerialVehicles.FighterJets.F15;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


public class HermesCheckFunctionTest {

    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();//init  aerial vehicles and missions.
    }


    //קבוצות שקילות
    @Test//78
    public void testCheckHermesEquilibriumGroupsInGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),78);//84 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 84 shouldn't be reset to 0 after Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 78);
    }


    @Test//299
    public void testCheckHermesEquilibriumGroupsAboveGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),123);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should  0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }


    //ערכי גבול

    @Test
    public void testCheckLimitsFighterJetsLowerBoundMiddle() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), -1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }


    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 1);
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundRight() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 101);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 101 should be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }
    @Test
    public void testCheckLimitsFighterJetsOnBound() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"), 100);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 101 should be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }

}
