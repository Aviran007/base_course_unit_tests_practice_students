package AIF.UAVs.Hermes;

import AIF.AIFUtil;
import AIF.AerialVehicles.UAVs.Hermes.Zik;
import AIF.Entities.Coordinates;
import AIF.Missions.AttackMission;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ZikSetMissionFunctionTest {
    public static AIFUtil aifUtil;
    public static Zik zik;
    public static Coordinates coordinatesToAttack;
    public static AttackMission attackMission;

    @BeforeClass
    public static void setUpBeforeClass() {

        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testZikExecutableMission() throws MissionTypeException {
        try {
            aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        } catch (MissionTypeException missionTypeException) {//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();

        }
    }

    @Test(expected = MissionTypeException.class)
    public void testZikNotExistsMission() throws MissionTypeException {

        aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("attack"));
    }



}
