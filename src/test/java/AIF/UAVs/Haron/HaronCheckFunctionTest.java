package AIF.UAVs.Haron;

//incomplete ...


import AIF.AIFUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class HaronCheckFunctionTest {

    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }


    //קבוצות שקילות
    @Test//78
    public void testCheckHermesEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"), 78);//84 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 84 shouldn't be reset to 0 after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 78);
    }


    @Test//299
    public void testCheckHermesEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"), 123);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should  0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 123);
    }


    @Test//23
    public void testCheckFighterJetsEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"), 23);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should  0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 23);
    }
    @Test
    public void testCheckUniqueZeroCasesFighterJets() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"), 0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 should throws an exception ? or reset to zero ?.", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 0);

    }
    @Test//-6
    public void testCheckFighterJetsEquilibriumGroupsUnderGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"), -6);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -6 should be reset to 0 by Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test//299
    public void testCheckFighterJetsEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"), 450);//1444 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 144 shouldn't be reset to 0 after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test//299
    public void testCheckFighterJetsEquilibriumGroupsOnGroup() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"), 150);//1444 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 144 shouldn't be reset to 0 after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }




}
