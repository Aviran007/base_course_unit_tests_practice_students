package AIF.FighterJets;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import AIF.AIFUtil;
import AIF.AerialVehicles.UAVs.Haron.Eitan;
import AIF.Entities.Coordinates;
import AIF.Missions.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EitanSetMissionFunctionTest {
    public static AIFUtil aifUtil;
    public static Eitan eitan;
    public static Coordinates coordinatesToAttack;
    public static AttackMission attackMission;

    @BeforeClass
    public static void setUpBeforeClass() {

        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testEitanExecutableMission() throws MissionTypeException {
        try {
            aifUtil.getAerialVehiclesHashMap().get("Etan").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException missionTypeException) {//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();

        }
    }

    @Test(expected = MissionTypeException.class)
    public void testEitanNotExistsMission() throws MissionTypeException {

        aifUtil.getAerialVehiclesHashMap().get("Etan").setMission(aifUtil.getAllMissions().get("bda"));
    }



}
